import firebase from 'firebase'
import 'firebase/auth'
import 'firebase/firestore'

const firebaseConfig = {
    apiKey: "AIzaSyAvdW7mnhfCY_VJB8VvjyebzqijjjUK5uk",
    authDomain: "pretest-project.firebaseapp.com",
    databaseURL: "https://pretest-project.firebaseio.com",
    projectId: "pretest-project",
    storageBucket: "pretest-project.appspot.com",
    messagingSenderId: "628418939620",
    appId: "1:628418939620:web:e6c1b7ef589167c5bf3af1"
  };

firebase.initializeApp(firebaseConfig)

export const firebaseAuth = firebase.auth()
export const firebaseDB = firebase.firestore()

